import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GraficaComponent } from './grafica/grafica.component';
import { HomeComponent } from './home/home.component';
import { InfoComponent } from './info/info.component';
import { NftComponent } from './nft/nft.component';
import { ConvertidorComponent } from './convertidor/convertidor.component';

const routes: Routes = [
  {path: 'grafica', component: GraficaComponent},
  {path: 'grafica/:coinUrl', component: GraficaComponent},
  {path: 'info', component: InfoComponent},
  {path: '', component: HomeComponent},
  {path: 'nft', component:NftComponent},
  {path: 'convertidor', component: ConvertidorComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
