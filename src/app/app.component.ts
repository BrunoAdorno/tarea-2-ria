import { Component, OnInit} from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { GraficaComponent } from './grafica/grafica.component';
import { BinanceService } from 'src/app/binance.service'
import { Router } from '@angular/router';

@Component({
   selector: 'app-root',
   templateUrl: './app.component.html',
   styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit{
   
   selectedCriptomoneda: string = '';

   lista = [
      {key: 'BTCUSDT', name: 'Bitcoin'},
      {key: 'ETHUSDT', name: 'Ethereum'},
      {key: 'LINKUSDT', name: 'ChainLink'},
      {key: 'ADAUSDT', name: 'Cardano'},
      {key: 'GRTUSDT', name: 'The Graph'},
      {key: 'DOGEUSDT', name: 'Dogecoin'},
      {key: 'XLMUSDT', name: 'Stellar'},
      {key: 'LRCUSDT', name: 'Loopring'},
      {key: 'AVAXUSDT', name: 'Avalanche'},
   ];
   
   constructor(private fb: FormBuilder, private router: Router){}

   coinForm = this.fb.group({
      pair: ["", Validators.required],
      timeframe: ["", Validators.required]
    });


   ngOnInit(): void {
      
   }

   redirect(){
      this.router.navigate(['/grafica/BTC']);
   }

   redirect2(){
      this.router.navigate(['/info']);
   }

   redirect3(){
      this.router.navigate(['/grafica']);
   }
   redirectNFT(){
      this.router.navigate(['/nft']);
   }

   redirectConvertidor(){
      this.router.navigate(['/convertidor']);
   }
   confirmarTodo(){
      console.log(this.selectedCriptomoneda);
      //console.log(this.selectedFecha);
      //if(window.confirm('¿Está seguro sus datos son correctos?')){
        //this.rest.completarPerfil(this.user.uid,this.user.email,this.selectedSexo,this.selectedFecha).subscribe((respuesta:boolean) => {
          //this.exito = respuesta;
         // console.log('PERFIL COMPLETADO: ', respuesta);
        //  this.router.navigate(['/']);
      //  });
      //}
    }
}

