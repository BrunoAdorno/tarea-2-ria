import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { webSocket } from 'rxjs/webSocket';

import {
  of ,
  Subscription
} from 'rxjs';
const BASE_URL = 'https://api.binance.com/api/v3'
const BASE_SOCKET = 'wss://stream.binance.com:9443/ws'
@Injectable({
  providedIn: 'root'
})
export class BinanceService {
  constructor(private http:HttpClient) { }
  
  getGrafica(symbol:String, interval:String, limit:number) {
    return this.http.get(`${BASE_URL}/klines?symbol=${symbol}&interval=${interval}&limit=${limit}`);
  }

  getPrecio(symbol:String) {
    return this.http.get(`${BASE_URL}/avgPrice?symbol=${symbol}`);
  }

  getTradeStream(symbol:string) {
    const realsymbol = symbol.toLowerCase();
    const params = `${realsymbol}@trade`;
    const subject = webSocket(`${BASE_SOCKET}/${params}`);
    
    return subject;
  }

  getLiveStream(symbol:string, interval:string) {
    const realsymbol = symbol.toLowerCase();
    const params = `${realsymbol}@kline_${interval}`;
    const subject = webSocket(`${BASE_SOCKET}/${params}`);
    
    return subject;
  }

  getPriceChangeStatistics(symbol:string){
    return this.http.get(`${BASE_URL}/ticker/24hr?symbol=${symbol}`);
  }
}


/*
1m
3m
5m
15m
30m
1h
2h
4h
6h
8h
12h
1d
3d
1w
1M

{
  "e": "kline",     // Event type
  "E": 123456789,   // Event time
  "s": "BNBBTC",    // Symbol
  "k": {
    "t": 123400000, // Kline start time
    "T": 123460000, // Kline close time
    "s": "BNBBTC",  // Symbol
    "i": "1m",      // Interval
    "f": 100,       // First trade ID
    "L": 200,       // Last trade ID
    "o": "0.0010",  // Open price
    "c": "0.0020",  // Close price
    "h": "0.0025",  // High price
    "l": "0.0015",  // Low price
    "v": "1000",    // Base asset volume
    "n": 100,       // Number of trades
    "x": false,     // Is this kline closed?
    "q": "1.0000",  // Quote asset volume
    "V": "500",     // Taker buy base asset volume
    "Q": "0.500",   // Taker buy quote asset volume
    "B": "123456"   // Ignore
  }
}
*/