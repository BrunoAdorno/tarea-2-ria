import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { BinanceService } from 'src/app/binance.service'
import { Router } from '@angular/router';
import { coinlist } from 'src/assets/coinlist'



@Component({
  selector: 'app-convertidor',
  templateUrl: './convertidor.component.html',
  styleUrls: ['./convertidor.component.css']
})
export class ConvertidorComponent implements OnInit {
  coinlist = coinlist;

  moneda1: string = 'ETHUSDT';
  monedanombre1: string = 'Ethereum';
  monedacode1: string = 'ETH';
  moneda2: string = 'BTCUSDT';
  monedanombre2: string = 'Bitcoin';
  monedacode2: string = 'BTC';
  cantidad : number = 0;
  cantidadform : number = 1;
  precio1: number = 0;
  precio2: number = 0;
  total: number = 0;
  conDecimal: string = '';

  constructor(private bs:BinanceService, private fb: FormBuilder, private router: Router){}


  coinForm = this.fb.group({
    pair: ["", Validators.required],
    timeframe: ["", Validators.required]
  });

  ngOnInit(): void {
    this.confirmarTodo();
  }


    confirmarTodo(){

      if((this.moneda2 != '') && (this.moneda1 != '')){
        //const valueInput = this.fondovalor.nativeElement.value
      this.cantidad = this.cantidadform;

      this.bs.getPrecio(this.moneda1).subscribe((d:any) => { 
      //console.log(d);
      this.precio1 = parseFloat(d.price); 
      console.log(this.precio1); 
        this.bs.getPrecio(this.moneda2).subscribe((d:any) => { 
          console.log(d);
          this.precio2 = parseFloat(d.price); 
          console.log(this.precio2); 
          
          this.total = (this.precio1 / this.precio2);

          this.total = (this.total * this.cantidad);
          this.conDecimal = this.total.toFixed(2);
          
          console.log(this.conDecimal);
          
          this.monedanombre1 = `${coinlist.find(c => c.key == this.monedacode1)?.name}`;
          this.monedanombre2 = `${coinlist.find(c => c.key == this.monedacode2)?.name}`;
        });
      });

      }
    }

    changeCoin1(coin: string) {
      this.monedacode1 = coin;
      this.moneda1 = coin + 'USDT';
    }

    changeCoin2(coin: string) {
      this.monedacode2 = coin;
      this.moneda2 = coin + 'USDT';
    }

}