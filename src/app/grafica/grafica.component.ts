import { Component, OnInit } from '@angular/core';
import { BinanceService } from 'src/app/binance.service';
import * as Highcharts from 'highcharts/highstock';
import * as TA from 'technicalindicators'
import { ActivatedRoute } from '@angular/router';
import { coinlist } from 'src/assets/coinlist';

@Component({
  selector: 'app-grafica',
  templateUrl: './grafica.component.html',
  styleUrls: ['./grafica.component.css']
})

export class GraficaComponent implements OnInit {

  ready: boolean = false;
  coinlist = coinlist;
  intervals = ["1M", "1w", "3d", "1d", "12h", "8h", "6h", "4h", "2h", "1h", "30m", "15m", "5m", "3m", "1m"];
  subject: any;
  tradeSubject: any;
  price: number = 0;
  data: any = [];
  volumeData: any = [];
  priceData: any = [];
  tradeData: any = [];
  secondaryData: any = [];
  chart: any;
  lowChart: any;
  coinCode: string = '';
  symbol: string = '';
  coin: string = '';
  interval: string = '5m';
  options: any = {
    chart: {
      animation: false,
      backgroundColor: bg,
    },
    colorAxis: {
      gridLineColor: line,
    },
    loading :{
      hideDuration: 0,
      showDuration: 0,
      style: {
        backgroundColor: '#000'
      }
    },
    navigator: {
      enabled: false,
    },
    rangeSelector: {
      enabled: false,
    },
    scrollbar:{
      enabled: false,
    },
    series: [
      {
        name: 'Precio',
        type: "candlestick",
        data: this.data,
        color: red,
        upColor: green,
        lineColor: red,
        upLineColor: green,
      },
      {
        name: `Volumen`,
        type: "column",
        data: this.secondaryData,
        color: volume,
        yAxis: 1,
      }
    ],
    time: {
      useUTC: false,
    },
    tooltip: {
      split: true
    },
    xAxis: {
      gridLineColor: line,
      labels: {
        style: {
          color: label,
        }
      }
    },
    yAxis: [{
      height: '80%',
      gridLineColor: line,
      opposite: false,
      labels: {
        style: {
          color: label,
        }
      }
    },
    {
      top: '80%',
      height: '20%',
      gridLineColor: line,
      opposite: false,
      labels: {
        enabled: false
      }
    }]
  }
  
  constructor(private bs:BinanceService, private route: ActivatedRoute){}
  ngOnInit(): void {
    this.ready = false;
    let coinUrl = this.route.snapshot.paramMap.get('coinUrl');
    this.coinCode = coinlist.find(c => c.key == coinUrl) ? `${coinUrl}` : 'BTC';
    this.symbol = `${this.coinCode}USDT`;
    this.coin = `${coinlist.find(p => p.key == this.coinCode)?.name}`;

    this.loadOldData(this.symbol, this.interval, 100);
    this.getLiveStream(this.symbol, this.interval);
    this.getTradeStream(this.symbol);
  }

  loadOldData(symbol:string, interval:string, limit:number) {
    this.chart = Highcharts.stockChart('container', this.options);
    this.chart.showLoading();
    this.bs.getGrafica(symbol, interval, limit).subscribe(d => { 
      this.data = Object.values(d).map(v => v.slice(0,-7).map((x: string) => parseFloat(x)));
      this.volumeData = Object.values(d).map(v => [v[0], parseFloat(v[5])]);
      this.priceData = Object.values(d).map(v => [v[0], parseFloat(v[4])]);
      
      this.chart = Highcharts.stockChart('container', this.options);

    });
  }

  getLiveStream(symbol:string, interval:string) {
    this.subject = this.bs.getLiveStream(symbol, interval);
    this.subject.subscribe((data: any) => {
      this.price = parseFloat(data.k.c);
      let row = [];
      let row2 = [];
      let row3 = [];
      row[0] = data.k.t;
      row[1] = parseFloat(data.k.o);
      row[2] = parseFloat(data.k.h);
      row[3] = parseFloat(data.k.l);
      row[4] = parseFloat(data.k.c);
      row2[0] = data.k.t;
      row2[1] = parseFloat(data.k.v);
      row3[0] = data.k.t;
      row3[1] = parseFloat(data.k.c);

      this.data[this.data.length-1] = row;
      this.volumeData[this.volumeData.length-1] = row2;
      this.priceData[this.priceData.length-1] = row3;
      if (data.k.x) {
        this.data.push(row);
        this.volumeData.push(row2);
        this.priceData.push(row3);
      }
      this.chart.update({
        series: [
          {
            data: this.data,
          },
          {
            data: this.volumeData,
          },
        ]
      })
    });
  }

  getTradeStream(symbol:string) {
    this.tradeData = [];
    this.tradeSubject = this.bs.getTradeStream(symbol)
    this.tradeSubject.subscribe((data: any) => {
      if (data.s == this.symbol) {
        this.tradeData.unshift([new Date(data.T).toLocaleTimeString(), parseFloat(data.p), parseFloat(data.q), data.m]);

        if (this.tradeData.length > 100) {
          this.tradeData.pop();
        }
      }
    });
  }
  
  changeInterval(interval:string) {
    this.ready = false;
    this.interval = interval;
    this.chart.destroy();
    this.loadOldData(this.symbol, this.interval, 100);
    this.subject.unsubscribe();
    this.getLiveStream(this.symbol, this.interval);
  }

  changeCoin(code:string) {
    this.ready = false;
    this.coinCode = code;
    this.price = 0;
    this.symbol = `${this.coinCode}USDT`;
    this.coin = `${coinlist.find(p => p.key == this.coinCode)?.name}`;
    this.subject.unsubscribe();
    this.tradeSubject.unsubscribe();
    this.chart.destroy();

    this.loadOldData(this.symbol, this.interval, 100);
    this.getTradeStream(this.symbol);
    this.getLiveStream(this.symbol, this.interval);
  }
}

const green = '#02c076';
const red  = '#cf304a';
const bg = '#191b20';
const line = '#22252e';
const label = '#efefef';
const volume = '#5c5c7c';