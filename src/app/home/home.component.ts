import { Component, OnInit } from '@angular/core';
import { BinanceService } from 'src/app/binance.service';
import { Router } from '@angular/router';
import { interval } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  
  
})
export class HomeComponent implements OnInit {
  ready: boolean = false;
  subject: any;
  coinArray: string[] = ['BTC', 'ETH', 'LINK', 'ADA', 'GRT', 'DOGE'];
  coinNames: string[] = ['Bitcoin', 'Ethereum', 'ChainLink', 'Cardano', 'The Graph', 'Dogecoin'];
  priceArray: number[] = [];
  priceChange: number[] = [];
  imagen1: String ="https://pbs.twimg.com/media/E3hrNMJUYA8YwJm?format=jpg&name=large";
  imagen2: String ="https://pbs.twimg.com/card_img/1402843799026429952/91td7kyH?format=jpg&name=small";
  imagen3: String = "https://pbs.twimg.com/media/E3hbNuAX0Agx418?format=jpg&name=large";
  ref1: String="https://twitter.com/binance/status/1402989373264564225";
  ref2: String="https://twitter.com/Binance_AUS/status/1402847738966933506";
  ref3: String="https://twitter.com/binance/status/1402972021571702787";
  active1: String="active";
  active2: String="";
  active3: String="";
  constructor(private bs:BinanceService, private router: Router){}
  ngOnInit(): void {
    this.coinArray.forEach(c => {
      this.priceArray.push(0);
      this.priceChange.push(0);
    })
    interval(6000).subscribe(x => {
      this.paginaFun();
  });
    this.getChanges();
    this.ready = false;
    this.getLiveStream();
    setTimeout(() => {this.ready = true}, 2000);
  }

  getChanges(){
    this.coinArray.forEach(coin => {
      this.bs.getPriceChangeStatistics(`${coin}USDT`).subscribe((data: any) => {
        this.priceChange[this.coinArray.indexOf(coin)] = parseFloat(data.priceChangePercent);
      });
    });
  }

  getLiveStream() {
    this.coinArray.forEach(coin => {
      this.subject = this.bs.getLiveStream(`${coin}USDT`, '1m');
      this.subject.subscribe((data: any) => {
        this.priceArray[this.coinArray.indexOf(coin)] = parseFloat(data.k.c);
      });
    });
  }

  redirect(simbolo:string){
    this.router.navigate([`/grafica/${simbolo}`]);
 }
paginaFun(){
  if(this.active1=="active"){
    this.paginaDos();
  } else
      if(this.active2=="active"){
        this.paginaTres();
  }else
      if(this.active3=="active"){
        this.paginaUno();
  }
}
 paginaUno(){
  this.imagen1="https://pbs.twimg.com/media/E3hrNMJUYA8YwJm?format=jpg&name=large";
  this.imagen2 ="https://pbs.twimg.com/card_img/1402843799026429952/91td7kyH?format=jpg&name=small";
  this.imagen3 = "https://pbs.twimg.com/media/E3hbNuAX0Agx418?format=jpg&name=large";
  this.ref1="https://twitter.com/binance/status/1402989373264564225";
  this.ref2="https://twitter.com/Binance_AUS/status/1402847738966933506";
  this.ref3="https://twitter.com/binance/status/1402972021571702787";
  this.active1="active";
  this.active2="";
  this.active3="";
 }

 paginaDos(){
  this.imagen1 ="https://pbs.twimg.com/media/E3hVsB5XwAY38Pq?format=jpg&name=large";
  this.imagen2 ="https://pbs.twimg.com/media/E3g_6XyXIAE3eZh?format=jpg&name=large";
  this.imagen3 ="https://pbs.twimg.com/media/E3gu9P6WYAcvbAC?format=jpg&name=large";
  this.ref1="https://twitter.com/binance/status/1402965621567111171";
  this.ref2="https://twitter.com/binance/status/1402941645855805441";
  this.ref3="https://twitter.com/binance/status/1402923258408837121";
  this.active1="";
  this.active2="active";
  this.active3="";
 }


 paginaTres(){
  this.imagen1 ="https://pbs.twimg.com/media/E3gawa9VgAA_EOU?format=jpg&name=large";
  this.imagen2 ="https://pbs.twimg.com/media/E3cLtiWXMAYnYDI?format=jpg&name=large";
  this.imagen3 ="https://pbs.twimg.com/media/E3byLgcWUAI1j6a?format=jpg&name=large";
  this.ref1="https://twitter.com/binance/status/1402900724644663297";
  this.ref2="https://twitter.com/binance/status/1402612114200666113";
  this.ref3="https://twitter.com/TheBinanceNFT/status/1402574681975234568";
  this.active1="";
  this.active2="";
  this.active3="active";
 }
}
