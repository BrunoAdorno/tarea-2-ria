import { Component, OnInit } from '@angular/core';
import ThreeWhiteSoldiers from 'technicalindicators/declarations/candlestick/ThreeWhiteSoldiers';

@Component({
  selector: 'app-nft',
  templateUrl: './nft.component.html',
  styleUrls: ['./nft.component.css']
})
export class NftComponent implements OnInit {
  pageItem1 = "page-item active";
  pageItem2 = "page-item";
  pageItem3 = "page-item";
  Row1 =["https://sorare.com/","https://s3.coinmarketcap.com/generated/nft/collections/sorare.png","Sorare","280,653","$61,831,888.15",	"$2,697,807.45",	"13,004",	"$61,831,888.15",	"347,785"];
  Row2= ["https://decentraland.org/","https://s3.coinmarketcap.com/generated/nft/collections/decentraland.png","Decentraland","174,941",	"$61,769,498.18",	"$1,284,834.36",	"314",	"$61,769,498.18",	"124,761"];
  Row3= ["https://www.sandbox.game/en/","https://s3.coinmarketcap.com/generated/nft/collections/thesandbox.png","The Sandbox","84,225",	"$22,216,238.81",	"$1,352,595.99",	"1,689",	"$22,216,238.81",	"91,376"];
  Row4= ["https://makersplace.com/","https://s3.coinmarketcap.com/generated/nft/collections/makersplace.png","MakersPlace","64,198",	"$20,171,280.18",	"$454,867.19",	"134",	"$20,171,280.18",	"16,330"];
  Row5= ["https://artblocks.io/","https://s3.coinmarketcap.com/generated/nft/collections/artblocks.png","Art Blocks","39,325",	"$27,207,248.22",	"$578,457.22",	"1,364",	"$27,207,248.22",	"51,142"];
  constructor() { }

  ngOnInit(): void {
    
  }
 
  ngAfterViewInit(): void {
    (<any>window).twttr.widgets.load();
}

  paginaUno(){
  this.Row1 = ["https://sorare.com/","https://s3.coinmarketcap.com/generated/nft/collections/sorare.png","Sorare","280,653",	"$61,831,888.15",	"$2,697,807.45",	"13,004",	"$61,831,888.15",	"347,785"];
  this.Row2= ["https://decentraland.org/","https://s3.coinmarketcap.com/generated/nft/collections/decentraland.png","Decentraland","174,941",	"$61,769,498.18",	"$1,284,834.36",	"314",	"$61,769,498.18",	"124,761"];
  this.Row3= ["https://www.sandbox.game/en/","https://s3.coinmarketcap.com/generated/nft/collections/thesandbox.png","The Sandbox","84,225",	"$22,216,238.81",	"$1,352,595.99",	"1,689",	"$22,216,238.81",	"91,376"];
  this.Row4= ["https://makersplace.com/","https://s3.coinmarketcap.com/generated/nft/collections/makersplace.png","MakersPlace","64,198",	"$20,171,280.18",	"$454,867.19",	"134",	"$20,171,280.18",	"16,330"];
  this.Row5= ["https://artblocks.io/","https://s3.coinmarketcap.com/generated/nft/collections/artblocks.png","Art Blocks","39,325",	"$27,207,248.22",	"$578,457.22",	"1,364",	"$27,207,248.22",	"51,142"];
  this.pageItem1 = "page-item active";
  this.pageItem2 = "page-item";
  this.pageItem3 = "page-item";
  }

  paginaDos(){
    this.Row1 =["https://ens.domains/es/","https://s3.coinmarketcap.com/generated/nft/collections/ethereumnameservice.png","Ethereum Name Service","414,601",	"$7,236,639.98",	"$230,960.59",	"6,659",	"$7,236,639.98",	"138,570"]
    this.Row2 =["https://www.megacryptopolis.com/","https://s3.coinmarketcap.com/generated/nft/collections/megacryptopolis.png","MegaCryptoPolis",	"120,847",	"$3,171,523.22",	"$127,556.35",	"81",	"$3,171,523.22",	"91,496"];
    this.Row3 =["https://www.csc-game.com/","https://s3.coinmarketcap.com/generated/nft/collections/cryptospacecommander.png","Crypto Space Commander",	"69,578",	"$1,316,724.26",	"$11,275.59",	"139",	"$1,316,724.26",	"25,423"];
    this.Row4 =["https://avastars.io/","https://s3.coinmarketcap.com/generated/nft/collections/avastars.png","Avastars",	"21,260",	"$5,140,282.90",	"$121,174.09",	"376",	"$5,140,282.90","25,096"];
    this.Row5 =["https://www.thehashmasks.com/","https://s3.coinmarketcap.com/generated/nft/collections/hashmasks.png","Hashmasks",	"14,864",	"$64,619,935.21",	"$181,831.64",	"97",	"$64,619,935.21",	"25,774"];
    this.pageItem1 = "page-item";
    this.pageItem2 ="page-item active";
    this.pageItem3 = "page-item";
  }  

  paginaTres(){
    this.Row1=["https://godsunchained.com/","https://s3.coinmarketcap.com/generated/nft/collections/godsunchained.png","Gods Unchained",	"7,583,078",	"$22,064,782.59",	"$63,154.20",	"1,636",	"$22,064,782.59",	"571,107"];
    this.Row2=["https://www.cryptokitties.co/","https://s3.coinmarketcap.com/generated/nft/collections/cryptokitties.png","CryptoKitties",	"2,000,556",	"$43,838,415.83",	"$136,160.76",	"2,201",	"$43,838,415.83",	"2,876,231",	];
    this.Row3=["https://www.mycryptoheroes.net/","https://s3.coinmarketcap.com/generated/nft/collections/mycryptoheroes.png","MyCryptoHeroes",	"58,021",	"$5,099,579.08",	"$3,945.96",	"61",	"$5,099,579.08",	"56,757"	];
    this.Row4=["https://www.f1deltatime.com/","https://s3.coinmarketcap.com/generated/nft/collections/f1deltatime.png","F1 Delta Time",	"31,606",	"$10,293,481.87",	"$8,215.55",	"89","$10,293,481.87",	"9,559"	];
    this.Row5=["https://urbit.org/","https://s3.coinmarketcap.com/generated/nft/collections/azimuthpoints.png","Urbit ID",	"14,794",	"$2,689,311.54",	"$62,897.07","25",	"$2,689,311.54",	"5,147"	];
    this.pageItem1 = "page-item";
    this.pageItem2 = "page-item";
    this.pageItem3 = "page-item active";
  }
}





